from django.contrib import admin

from .models import Article,Comment  # .models olması bu dosyadaki models den yükle anlamında
#admin.site.register(Article)
admin.site.register(Comment)
@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ["title","author","created_date"]
    list_display_links = ["title","created_date"]
    search_fields = ["title"]
    list_filter = ["created_date"]
    ordering = ["created_date"]
    class Meta:
        model = Article