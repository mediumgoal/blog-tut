from django.contrib import admin
from django.urls import path
from . import views # şuanki klasördekki viewsi al
app_name = "user"

urlpatterns = [
    path('',views.userIndex, name = "userIndex"),
    path('register/',views.register, name = "register"),
    path('login/',views.loginUser, name = "login"),
    path('logout/',views.logoutUser, name = "logout")

]