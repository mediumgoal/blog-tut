from django.shortcuts import render, HttpResponse,redirect
from .forms import RegisterForm
from .forms import LoginForm
from django.contrib import messages
from django.contrib.auth.models import User # user olayları
from django.contrib.auth import login,authenticate,logout # session olayı için, auth da kontrol için


def userIndex(request):
    return HttpResponse("<p>User bilgileri burada gözükebilir</p>")

def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid(): #clean metodu çağrıldı
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            
            newUser = User(username = username)
            newUser.set_password(password)

            newUser.save()
            login(request,newUser)
            messages.add_message(request, messages.SUCCESS, 'Başarıyla kayıt oldunuz')
            return redirect("index")
        messages.add_message(request, messages.WARNING, 'Bir hata oluştu.')
        context = {"form":form}
        return render(request,"register.html",context)
    else:
        form = RegisterForm()
        context = {"form":form}
        return render(request,"register.html",context)
    """form = RegisterForm()
    context = {
        "form":form
    }
    return render(request,"register.html",context)
    """
def loginUser(request):
    form = LoginForm(request.POST or None)

    context = {
        "form":form
    }

    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")

        user = authenticate(username = username,password = password)

        if user is None:
            messages.add_message(request, messages.WARNING, 'Kullanıcı adı veya şifre hatalı.')
            return render(request,"login.html",context)

        messages.add_message(request, messages.SUCCESS, 'Başarıyla giriş yaptınız')
        login(request,user)
        return redirect("index")
    return render(request,"login.html",context)

def logoutUser(request):
    logout(request)
    messages.add_message(request, messages.SUCCESS, "Başarıyla çıkış yaptınız")
    return redirect("index")

